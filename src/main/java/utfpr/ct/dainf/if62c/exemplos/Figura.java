package utfpr.ct.dainf.if62c.exemplos;

/**
 * Programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public interface Figura {
    String getNome();
    double getPerimetro();
    double getArea();
}
